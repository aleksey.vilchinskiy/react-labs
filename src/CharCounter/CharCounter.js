import React, {Component} from 'react'
import './CharCounter.css'

class CharCounter extends Component {
    render() {
        const {value} = this.props;
        return <div className="charCounter">{value}</div>
    }
}

export default CharCounter