import React, {Component} from 'react'
import './ValidationComponent.css'

class ValidationComponent extends Component {

    minValue = 5;
    maxValue = 10;
    render() {
        let text = 'Enough';
        if (this.minValue > this.props.value) {
            text = 'Less than minimum';
        } else if (this.maxValue < this.props.value) {
            text = 'more than maximum';
        }
        return <div className="validationComponent">{text}</div>
    }
}

export default ValidationComponent