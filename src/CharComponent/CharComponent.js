import React, {Component} from 'react'
import './CharComponent.css'

class CharComponent extends Component {

    render() {
        const {click, value} = this.props;

        return <div className="charComponent" onClick={click}>{value}</div>
    }
}

export default CharComponent